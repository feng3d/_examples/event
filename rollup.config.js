import fs from 'fs';
import path from 'path';
import transpile from 'rollup-plugin-buble';
import commonjs from 'rollup-plugin-commonjs';
import json from 'rollup-plugin-json';
import resolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';
import sourcemaps from 'rollup-plugin-sourcemaps';
import { string } from 'rollup-plugin-string';
import { terser } from 'rollup-plugin-terser';
import typescript from 'rollup-plugin-typescript';
import pkg from './package.json';

async function main()
{
    const plugins = [
        sourcemaps(),
        json(),
        resolve({
            browser: true,
            preferBuiltins: false,
        }),
        commonjs({}),
        typescript(),
        string({
            include: [
                '**/*.glsl',
            ],
        }),
        replace({
            __VERSION__: pkg.version,
        }),
        transpile(),
    ];
    if (process.env.NODE_ENV === 'production')
    {
        plugins.push(terser({
            output: {
                comments: (node, comment) => comment.line === 1,
            },
        }));
    }

    const compiled = (new Date()).toUTCString().replace(/GMT/g, 'UTC');
    const sourcemap = (pkg.sourcemap === undefined || pkg.sourcemap === true);
    const results = [];

    const banner = [
        `/*!`,
        ` * ${pkg.name} - v${pkg.version}`,
        ` * Compiled ${compiled}`,
        ` * Author ${pkg.author}`,
        ` * Git ${pkg.repository.url}`,
        ` *`,
        ` * ${pkg.name} is licensed under the MIT License.`,
        ` * http://www.opensource.org/licenses/mit-license`,
        ` */`,
    ].join('\n');

    // 输入目录
    const inputBasePath = path.relative(__dirname, 'src');
    // 输出目录
    const outputBasePath = path.relative(__dirname, 'public/dist');
    const files = fs.readdirSync('./src').filter((p) => p.includes('.ts'));

    files.forEach((tsfile) =>
    {
        const input = path.join(inputBasePath, tsfile);
        const file = path.join(outputBasePath, tsfile.replace(/\.ts/, '.js'));

        results.push({
            input,
            output: {
                banner,
                file,
                format: 'iife',
                sourcemap,
            },
            treeshake: false,
            plugins,
        });
    });

    return results;
}

export default main();
