import { EventEmitter } from '@feng3d/event';
export { };

interface DisplayObjectEventMap
{
    removed: Container;
    added: Container;
}

class DisplayObject<T extends DisplayObjectEventMap = DisplayObjectEventMap> extends EventEmitter<T>
{
    constructor()
    {
        super();
        this.on('added', () => { });
        this.on('removed', () => { });
    }
}

interface ContainerEventMap extends DisplayObjectEventMap
{
    childAdded: { child: DisplayObject, parent: Container, index: number }
    childRemoved: { child: DisplayObject, parent: Container, index: number }
}

class Container<T extends ContainerEventMap = ContainerEventMap> extends DisplayObject<T>
{
    constructor()
    {
        super();
        this.on('added', () => { });
        this.on('removed', () => { });
        this.on('childAdded', () => { });
        this.on('childRemoved', () => { });
    }
}

const eventEmitter = new EventEmitter();
eventEmitter.on('abc', () => { });

const object = new DisplayObject();
const container = new Container();

object.on('added', () => { });
object.on('removed', () => { });
object.onAny((e) =>
{
    e.data;
});

container.on('added', () => { }); // 继承自DisplayObject的事件
container.on('childAdded', () => { });
