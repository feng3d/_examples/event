/* eslint-disable @typescript-eslint/no-empty-function */
import { EventEmitter } from '@feng3d/event';
export { };

interface AEventMap
{
    a: { a: number };
}

class A<T extends AEventMap = AEventMap> extends EventEmitter<T>
{
    a = 1;
    constructor()
    {
        super();
        this.on('a', () => { });
        // this.on('a1', () => { });
    }
}

interface BEventMap extends AEventMap
{
    b: { b: boolean };
}

class B<T extends BEventMap = BEventMap> extends A<T>
{
    b = true;
    constructor()
    {
        super();
        this.on('a', () => { });
        this.on('b', () => { });
        // this.on('c', () => { });
    }
}

const e = new EventEmitter();
e.on('abc', () => { });

const a = new A();
a.on('a', () => { });

const b = new B();
b.on('a', () => { });
b.on('b', () => { });
// b.on('c', () => { });
